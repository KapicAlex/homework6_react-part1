import React from "react";
import { CircularProgress } from "@material-ui/core";
import "./preloader.css"

class Preloader extends React.Component {
  render() {
    return (
      <div className="preloader" style={{display: this.props.getData ? "none" : "block"}}>
        <CircularProgress size="200px" />
      </div>
    )
  }
}

export default Preloader;