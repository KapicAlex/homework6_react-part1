import React from "react";
import "./ownMessage.css";

class OwnMesssage extends React.Component {
  render() {
    const {id, text, createdAt, editedAt} = this.props.message;
    const time = editedAt ? new Date(editedAt) : new Date(createdAt);
    const deleteMessage = this.props.delete;
    const editMessage = this.props.edit;

    return (
      <div className="own-message">
        <div className="message-text">{text}</div>
        <div className="message-time">{time.toLocaleTimeString("ru-Ru", {timeStyle: 'short'})}</div>
        <div className="message-controls">
          <button className="message-edit" onClick={() => editMessage(id)}><i className="fas fa-edit"></i></button>
          <button className="message-delete" onClick={() => deleteMessage(id)}><i className="fas fa-trash-alt"></i></button>
        </div>
      </div>
    )
  }
}

export default OwnMesssage;