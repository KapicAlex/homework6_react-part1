import React from "react";
import { v4 as uuidv4 } from 'uuid';
import "./messageInput.css";

class MessageInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messageText: this.props.needEdit === false ? "" : this.props.needEdit.text
    }
    this.onChange=this.onChange.bind(this);
    this.addText=this.addText.bind(this);
    this.editText=this.editText.bind(this);
  }

  onChange(e) {
    const value = e.target.value;
    this.setState({
      messageText: value,
    });
  }

  addText(e) {
    e.preventDefault();

    if(this.state.messageText) {
      const message = {
        id: uuidv4(),
        userId: this.props.data.ownId,
        avatar: this.props.data.avatar,
        user: this.props.data.ownName,
        text: this.state.messageText,
        createdAt: Date.now(),
        editedAt: "",
      };
      this.props.add(message);
      this.setState({
        messageText: "",
      })
    }
  }

  editText(e) {
    e.preventDefault();
    
    if(this.state.messageText) {
      const message = {
        ...this.props.needEdit,
        text: this.state.messageText,
        editedAt: Date.now(),
      }
      this.props.completeEdit(message);
      this.setState({
        messageText: "",
      })
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.needEdit !== this.props.needEdit && !prevProps.needEdit) {
      this.setState({messageText: this.props.needEdit.text});
    }
  }

  render() {
    const isEdit = this.props.needEdit;

    return (
      <div className="message-input" >
        <form onSubmit={isEdit !== false ? this.editText : this.addText}>
          <input type="text"
            className="message-input-text"
            placeholder="Enter your message"
            value={this.state.messageText}
            onChange={this.onChange} />
          <button className="message-input-button"
            type="submit">Send</button>
        </form>
      </div>
    )
  }
}

export default MessageInput;