import React from "react";
import moment from 'moment';
import Header from "./Header/header";
import Preloader from "./Preloader/preloader";
import MessageList from "./MessageList/messageList";
import MessageInput from "./MessageInput/messageInput";
import "./chat.css";

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      getData: false,
      ownName: "Binary Academy Guru",
      ownId: "SUPERADMIN",
      avatar: "https://avatarfiles.alphacoders.com/238/238245.jpg",
      needEdit: false,
    };
    this.addMessage=this.addMessage.bind(this);
    this.deleteMessage=this.deleteMessage.bind(this);
    this.editMessage=this.editMessage.bind(this);
    this.completeEdit=this.completeEdit.bind(this);
  }

  getMainInfo(messagesInfo) {
    const allUserId = messagesInfo.map(message => message.userId);
    const allUsers = new Set(allUserId);
    const lastMessageDate =  messagesInfo[messagesInfo.length - 1]?.createdAt;
    return {
      usersSum: allUsers.size,
      allMessages: allUserId.length,
      lastMessageDate: moment(lastMessageDate).format("DD.MM.YYYY HH:mm"),
    }
  }

  addMessage(message){
    this.setState({...this.state, data: [...this.state.data, message]});
  }

  deleteMessage(id) {
    const newMessages = this.state.data.filter(message => message.id !== id);
    this.setState({...this.state, data: newMessages});
  }

  editMessage(id) {
    const oldMessage = this.state.data.find(message => message.id === id);
    this.setState({...this.state, needEdit: oldMessage})
  }

  completeEdit(messageNew) {
    const newData = this.state.data.map(message => {
      if(message.id === messageNew.id) {
        return messageNew;
      }
      return message;
    })
    this.setState({...this.state, data: newData, needEdit: false});
  }
  

  async componentDidMount() {
    await fetch(this.props.url)
            .then(response => response.json())
            .then(data => {
              this.setState({...this.state, data: data, getData: true});
            })
            .catch(err => console.log(err));
  }

  render() {
    return (
      <main className="chat">
        <Preloader getData = {this.state.getData} />
        <Header data = {this.getMainInfo(this.state.data)}/>
        <MessageList messages = {this.state.data}
                    userId = {this.state.ownId}
                    deleteMessage={this.deleteMessage}
                    editMessage={this.editMessage}
                    needEdit={this.state.needEdit} />
        <MessageInput add={this.addMessage}
                      data = {this.state}
                      needEdit={this.state.needEdit}
                      completeEdit={this.completeEdit} />
      </main>
    )
  }
}

export default Chat;