import React from "react";
import "./header.css"

class Header extends React.Component {
  render() {
    const {usersSum, allMessages, lastMessageDate} = this.props.data;
    
    return (
      <div className="header">
        <h1 className="header-title">My chat</h1>
        <div className="header-users-count">{usersSum} participants</div>
        <div className="header-messages-count">{allMessages} messages</div>
        <div className="header-last-message-date">last message at {lastMessageDate}</div>
      </div>
    )
  }
}

export default Header;