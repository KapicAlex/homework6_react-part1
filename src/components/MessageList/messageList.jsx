import React from "react";
import moment from 'moment';
import "./messageList.css";
import Message from "../Message/message";
import OwnMesssage from "../OwnMessage/ownMessage";
import Divider from "../Divider/divider";

class MessageList extends React.Component {

  render() {
    const messages = this.props.messages;
    const userId = this.props.userId;
    const deleteMessage = this.props.deleteMessage;
    const editMessage = this.props.editMessage;

    return (
      <div className="message-list">

        {messages.map((message, idx) => {
          let temp = [];
          const prevDate = idx === 0 ? 1 : messages[idx-1].createdAt;
          const currentDate = message.createdAt;

          if(message.userId === userId) {

            if(prevDate && moment(prevDate).calendar() !== moment(currentDate).calendar()) {
              temp.push(<Divider key={message.createdAt} date={message.createdAt} />);
            }
            temp.push(<OwnMesssage key={message.id} message={message} delete={deleteMessage} edit={editMessage} />);
            return temp
          }

          if(prevDate && moment(prevDate).calendar() !== moment(currentDate).calendar()){
            temp.push(<Divider key={message.createdAt} date={message.createdAt} />)
          }
          
          temp.push(<Message key={message.id} message={message} />);
          return temp
        })}

      </div>
    )
  }
}

export default MessageList;