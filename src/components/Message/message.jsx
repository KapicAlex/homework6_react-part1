import React from "react";
import "./message.css";

class Message extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLike: false,
    }
  }

  makeLike = () => {
    this.setState({isLike: !this.state.isLike})
  }

  render() {
    const {avatar, user, text, createdAt, editedAt} = this.props.message;
    const time = editedAt ? new Date(editedAt) : new Date(createdAt);
    
    return (
      <div className="message">
        <img src={avatar} alt="avatar" className="message-user-avatar" />
        <div className="message-main">
          <div className="message-user-name">{user}</div>
          <div className="message-text">{text}</div>
          <div className="message-time">{time.toLocaleTimeString("ru-Ru", {timeStyle: 'short'})}</div>
          <button className={this.state.isLike ? "message-liked" : "message-like"} onClick={this.makeLike}>
            <i className="fas fa-thumbs-up"></i>
          </button>
        </div>
      </div>
    )
  }
}

export default Message;